<?php

class Gateway_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function _checkESP($esp, $api){

    	$q = $this->db->query('SELECT * FROM sensors WHERE sensor_no = ? AND api_key = ?', array($esp, $api));

    	if ($q->num_rows()){
    		return $q->row_array()['description'];
    	} else {
    		return false;
    	}
	}
	
	public function _checkupdate($esp, $firmware) {
		$q = $this->db->query('SELECT filename, firmware FROM updates WHERE sensor = ? AND status = \'pending\' ORDER BY timestamp ASC LIMIT 1', array($esp));

		if ($q->num_rows()){
			if ($q->row_array()['firmware'] != $firmware){
				return $q->row_array()['filename'];
			} else {
				return false;
			}
    	} else {
    		return false;
    	}  
	}

	public function _confirmUpdate($esp, $status) {
		$this->db->where('sensor', $esp);
		$this->db->where('status', 'pending');
		return $this->db->update('updates', array('status' => $status));
	}

    public function _heartbit($esp, $data){

    	$this->db->where('sensor_no', $esp);
    	return $this->db->update('sensors', $data); 
	}
	
	public function _savedata($data)
	{
		return $this->db->insert('values', $data);
	}

}
