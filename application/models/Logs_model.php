<?php

class Logs_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function _save_log($chip_id, $action) {
        return $this->db->insert('_logs', array('sensor_id' => $chip_id,
                                                'action'    => $action,
                                                'ip_address'=> $_SERVER['REMOTE_ADDR'],
                                                'host_name' => gethostbyaddr($_SERVER['REMOTE_ADDR']))
                                );
    }

}
