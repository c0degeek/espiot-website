<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gateway extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
		$this->load->model('gateway_model', 'gateway');
		$this->load->helper('file');
		$this->load->helper('download');
		//$this->output->enable_profiler(TRUE);
    }


	public function index()
	{
		http_response_code(403);
	}

	public function update()
    {

    	if (@$_SERVER['HTTP_USER_AGENT'] == 'ESP8266-http-Update'){

    		$data = array(	'sdk_version' 		=> $_SERVER['HTTP_X_ESP8266_SDK_VERSION'],
    						'software_version'	=> $_SERVER['HTTP_X_ESP8266_VERSION'],
    						'heartbit' 			=> date("Y-m-d H:i:s"));

    		$this->gateway->_heartbit($_SERVER['HTTP_X_ESP8266_CHIP_ID'], $data);
			$this->logs->_save_log($_SERVER['HTTP_X_ESP8266_CHIP_ID'], 'update');

			if ($firmware = $this->gateway->_checkupdate($_SERVER['HTTP_X_ESP8266_CHIP_ID'], $_SERVER['HTTP_X_ESP8266_VERSION'])) {
				force_download($_SERVER['DOCUMENT_ROOT'] . '/_firmwares/' . $firmware, NULL);	
			} else {
				http_response_code(304);
			}	

    	} else {
    		http_response_code(403);
    	}
        		
	}
	
	public function request() {
		if (@$_SERVER['HTTP_USER_AGENT'] == 'ESP8266HTTPClient' && @$_POST['status']){
			
			$esp_id = @$_SERVER['PHP_AUTH_USER'];
			$esp_api_key = @$_SERVER['PHP_AUTH_PW'];

			if($this->gateway->_checkESP($esp_id, $esp_api_key)){
				$this->gateway->_confirmUpdate($esp_id, $_POST['status']);
			}
		} else {
			http_response_code(403);
		}
	}

	public function savedata() 
	{
    	if (@$_SERVER['HTTP_USER_AGENT'] == 'ESP8266HTTPClient'){

			$this->logs->_save_log($_SERVER['PHP_AUTH_USER'], 'savedata');

    		$esp_id = @$_SERVER['PHP_AUTH_USER'];
			$esp_api_key = @$_SERVER['PHP_AUTH_PW'];

			if($descr = $this->gateway->_checkESP($esp_id, $esp_api_key)){

				if (isset($_POST['temp']) && isset($_POST['voltage'])){

					$data = array(	'sensor'  => $esp_id, 
									'measure_descr' => $descr,
									'value'   => $_POST['temp'],
									'voltage' => $_POST['voltage']);

					if ($this->gateway->_savedata($data)){

						http_response_code(201); #OK
					
					} else {
						http_response_code(500); #Database Error
					}

				} else {
					http_response_code(400); #Brak parametrów
				}

			} else {
				http_response_code(406); #Urządzenie niezarejestorwane
			}

		} else {
			http_response_code(403); #Nieprawidłowy agent
		}

	}

}
